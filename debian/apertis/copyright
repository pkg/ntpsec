Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: attic/*
Copyright: no-info-found
License: BSD-2-clause

Files: contrib/*
Copyright: no-info-found
License: BSD-2-clause

Files: debian/*
Copyright: Bdale Garbee <bdale@gag.com>
            Bernhard Schmidt <berni@debian.org>
            Bruce Walker <w1bw@debian.org>
            Dimitri John Ledkov <xnox@ubuntu.com>
            Kurt Roeckx <kurt@roeckx.be>
            Matthias Urlichs <smurf@debian.org>
            Peter Eisentraut <petere@debian.org>
            Richard Laager <rlaager@debian.org>
License: NTP

Files: debian/apparmor-profile
Copyright: 2009-2012, Canonical Ltd.
 2002-2005, Novell/SUSE
License: GPL-2

Files: debian/apparmor-profile.tunable
Copyright: 2011, Canonical, Ltd.
 2002-2005, Novell/SUSE
License: GPL-2

Files: devel/hacking.adoc
Copyright: no-info-found
License: BSD-3-clause and/or NTP

Files: devel/linkcheck
Copyright: no-info-found
License: BSD-2-clause

Files: docs/*
Copyright: no-info-found
License: GPL

Files: docs/copyright.adoc
Copyright: University of Delaware
License: BSD-2-clause and/or NTP

Files: docs/discover.adoc
Copyright: no-info-found
License: NTP

Files: include/*
Copyright: the NTPsec project contributors
 Frank Kardel <kardel <AT> ntp.org>
License: BSD-3-clause

Files: include/isc_interfaceiter.h
 include/isc_netaddr.h
 include/isc_result.h
 include/ntp_assert.h
Copyright: the NTPsec project contributors
 Internet Systems Consortium, Inc. ("ISC")
 Internet Software Consortium.
License: ISC

Files: include/mbg_gps166.h
Copyright: the NTPsec project contributors
 Meinberg Funkuhren (www.meinberg.de)
 Frank Kardel <kardel <AT> ntp.org>
License: BSD-3-clause

Files: include/ntp_debug.h
Copyright: the NTPsec project contributors
 Frank Kardel
License: BSD-2-clause

Files: include/ntp_endian.h
 include/nts.h
 include/nts2.h
Copyright: the NTPsec project contributors
License: BSD-2-clause

Files: include/ntp_filegen.h
Copyright: Rainer Pruy
License: BSD-2-clause

Files: include/parse.h
 include/parse_conf.h
Copyright: the NTPsec project contributors
 Frank Kardel <kardel@ntp.org>
License: BSD-3-clause

Files: include/timespecops.h
Copyright: the NTPsec project contributors
License: NTP

Files: include/timetoa.h
Copyright: the NTPsec project contributors
 Juergen Perlinger <perlinger@ntp.org> for the NTP project.
License: NTP

Files: libaes_siv/*
Copyright: no-info-found
License: Apache-2.0

Files: libaes_siv/aes_siv.c
 libaes_siv/aes_siv.h
 libaes_siv/bench.c
 libaes_siv/demo.c
 libaes_siv/tests.c
Copyright: Akamai Technologies, Inc.
License: Apache-2.0

Files: libjsmn/*
Copyright: Serge A. Zaitsev
License: Expat

Files: libjsmn/README.md
Copyright: no-info-found
License: Expat

Files: libntp/*
Copyright: the NTPsec project contributors
License: BSD-2-clause

Files: libntp/assert.c
 libntp/isc_interfaceiter.c
 libntp/isc_net.c
Copyright: the NTPsec project contributors
 Internet Systems Consortium, Inc. ("ISC")
 Internet Software Consortium.
License: ISC

Files: libntp/emalloc.c
Copyright: Otto Moerbeek <otto@drijf.net>
License: ISC

Files: libntp/ntp_calendar.c
Copyright: the NTPsec project contributors
 Juergen Perlinger <perlinger@ntp.org> for the NTP project.
License: NTP

Files: libntp/strl_obsd.c
Copyright: the NTPsec project contributors
 Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: libntp/timespecops.c
Copyright: the NTPsec project contributors
 Juergen Perlinger (perlinger@ntp.org)
License: NTP

Files: libparse/*
Copyright: the NTPsec project contributors
 Frank Kardel <kardel@ntp.org>
License: BSD-3-clause

Files: libparse/binio.c
 libparse/clk_rawdcf.c
 libparse/clk_trimtsip.c
 libparse/data_mbg.c
 libparse/ieee754io.c
Copyright: the NTPsec project contributors
 Frank Kardel <kardel <AT> ntp.org>
License: BSD-3-clause

Files: libparse/clk_computime.c
Copyright: the NTPsec project contributors
 Frank Kardel <kardel <AT> ntp.org>
 Alois Camenzind <alois.camenzind@ubs.ch>
License: BSD-3-clause

Files: libparse/clk_wharton.c
Copyright: Philippe De Muyter <phdm@macqel.be>
License: BSD-2-clause

Files: ntpclients/*
Copyright: no-info-found
License: BSD-2-clause

Files: ntpclients/ntpleapfetch
Copyright: 2014, Timothe Litt litt at acm dot org
License: BSD-2-clause

Files: ntpd/*
Copyright: the NTPsec project contributors
License: BSD-2-clause

Files: ntpd/ntp_config.c
 ntpd/ntp_parser.y
 ntpd/ntp_scanner.c
 ntpd/ntp_scanner.h
Copyright: the NTPsec project contributors
 Sachin Kamboj
License: BSD-2-clause

Files: ntpd/ntp_filegen.c
Copyright: the NTPsec Project contributors
 Rainer Pruy
License: BSD-2-clause

Files: ntpd/ntp_leapsec.c
 ntpd/ntp_leapsec.h
 ntpd/refclock_gpsd.c
Copyright: the NTPsec project contributors
 Juergen Perlinger <perlinger@ntp.org>
License: NTP

Files: ntpd/ntp_sandbox.c
Copyright: no-info-found
License: BSD-2-clause

Files: ntpd/refclock_generic.c
Copyright: the NTPsec project contributors
 Frank Kardel <kardel@ntp.org>
License: BSD-3-clause

Files: ntpd/refclock_jjy.c
Copyright: Takao Abe.
License: BSD-3-clause

Files: ntpd/refclock_oncore.c
Copyright: 1991-1997, MOTOROLA INC.
License: Beerware

Files: ntpd/refclock_trimble.c
Copyright: the NTPsec project contributors
 Trimble Navigation Ltd.
License: BSD-4-clause

Files: ntpfrob/*
Copyright: the NTPsec project contributors
License: BSD-2-clause

Files: ntpfrob/pps-api.c
Copyright: the NTPsec project contributors
 Poul-Henning Kemp.
License: BSD-2-clause

Files: packaging/SUSE/ntpsec.spec
Copyright: SUSE LINUX GmbH, Nuernberg, Germany.
 Malcolm J Lewis <malcolmlewis@opensuse.org>
License: BSD-2-clause

Files: pylib/*
Copyright: no-info-found
License: BSD-2-clause

Files: pylib/ntp-in.egg-info
Copyright: no-info-found
License: Beerware

Files: tests/*
Copyright: no-info-found
License: BSD-2-clause

Files: tests/unity/*
Copyright: 2007-2019, Mike Karlesky, Mark VanderVoord, Greg Williams
License: Expat

Files: tests/unity/unity_fixture.c
 tests/unity/unity_fixture.h
 tests/unity/unity_fixture_internals.h
Copyright: 2010, James Grenning and Contributed to Unity Project
 2007, Mike Karlesky, Mark VanderVoord, Greg Williams
License: Expat

Files: waf
Copyright: no-info-found
License: BSD-3-clause

Files: attic/ntpver build/* devel/* devel/CommitLog-4.1.0 devel/ntpv5.adoc devel/trace/* docs/driver_modem.adoc docs/includes/* docs/pic/* docs/pic/alice11.gif docs/pic/alice13.gif docs/pic/alice15.gif docs/pic/alice23.gif docs/pic/alice32.gif docs/pic/alice35.gif docs/pic/alice38.gif docs/pic/alice44.gif docs/pic/alice47.gif docs/pic/alice51.gif docs/pic/alice61.gif docs/pic/barnstable.gif docs/pic/beaver.gif docs/pic/boom3.gif docs/pic/boom3a.gif docs/pic/boom4.gif docs/pic/broad.gif docs/pic/bustardfly.gif docs/pic/discipline.gif docs/pic/dogsnake.gif docs/pic/driver_palisade.gif docs/pic/fg6021.gif docs/pic/fig_3_1.gif docs/pic/flatheads.gif docs/pic/flt1.gif docs/pic/flt2.gif docs/pic/flt4.gif docs/pic/flt5.gif docs/pic/flt6.gif docs/pic/flt7.gif docs/pic/flt8.gif docs/pic/flt9.gif docs/pic/hornraba.gif docs/pic/igclock.gif docs/pic/neoclock4x.gif docs/pic/oncore_evalbig.gif docs/pic/oncore_utplusbig.gif docs/pic/orchestra.gif docs/pic/oz2.gif docs/pic/panda.gif docs/pic/pd_om006.gif docs/pic/pd_om011.gif docs/pic/peer.gif docs/pic/pogo.gif docs/pic/pogo1a.gif docs/pic/pogo3a.gif docs/pic/pogo5.gif docs/pic/pogo6.gif docs/pic/pogo7.gif docs/pic/pogocell.gif docs/pic/stats.gif docs/pic/sx5.gif docs/pic/time1.gif docs/pic/tonea.gif docs/pic/wingdorothy.gif include/ntp_stdlib.h libparse/clk_sel240x.c ntpd/refclock_modem.c packaging/*
Copyright: Network Time Foundation
 NTPsec project contributors
 Mark Andrews <mark_andrews@isc.org>
 Leitch atomic clock controller
 Bernd Altmeier <altmeier@atlsoft.de>
 hopf Elektronik serial line and PCI-bus devices
 Viraj Bais <vbais@mailman1.intel.com>
 Nelson B Bolyard <nelson@bolyard.me>
 update and complete broadcast and crypto features in sntp
 Michael Barone <michael,barone@lmco.com>
 GPSVME fixes
 Jean-Francois Boudreault <Jean-Francois.Boudreault@viagenie.qc.ca>
 IPv6 support
 Karl Berry <karl@owl.HQ.ileaf.com>
 syslog to file option
 Greg Brackley <greg.brackley@bigfoot.com>
 Major rework of WINNT port.
 Clean up recvbuf and iosignal code into separate modules.
 Marc Brett <Marc.Brett@westgeo.com>
 Magnavox GPS clock driver (removed in NTPsec)
 Piete Brooks <Piete.Brooks@cl.cam.ac.uk>
 MSF clock driver, Trimble PARSE support
 Reg Clemens <reg@dwf.com>
 Oncore driver
 Steve Clift <clift@ml.csiro.au>
 OMEGA clock driver
 Casey Crellin <casey@csc.co.za>
 vxWorks (Tornado) port and help with target configuration
 Sven Dietrich <sven_dietrich@trimble.com>
 Palisade reference clock driver, NT adj. residuals, integrated
 Greg's Winnt port.
 John A. Dundas III <dundas@salt.jpl.nasa.gov>
 Apple A/UX port
 Torsten Duwe <duwe@immd4.informatik.uni-erlangen.de>
 Linux port
 Dennis Ferguson <dennis@mrbill.canet.ca>
 foundation code for NTP Version 2 as specified in RFC-1119
 Daniel Fox Franke <dfranke@dfranke.us>
 CVE fixes, protocol machine refactoring
 John Hay <jhay@@icomtek.csir.co.za>
 IPv6 support and testing
 Dave Hart <davehart@davehart.com>
 General maintenance
 Damon Hart-Davis <d@hd.org>
 ARCRON MSF clock driver
 Claas Hilbrecht <neoclock4x@linum.com>
 NeoClock4X clock driver
 Glenn Hollinger <glenn@herald.usask.ca>
 GOES clock driver (removed in NTPsec)
 Mike Iglesias <iglesias@uci.edu>
 DEC Alpha port
 Jim Jagielski <jim@jagubox.gsfc.nasa.gov>
 A/UX port
 Jeff Johnson <jbj@chatham.usdesign.com>
 massive prototyping overhaul
 William L. Jones <jones@hermes.chpc.utexas.edu>
 RS/6000 AIX modifications, HPUX modifications
 Poul-Henning Kamp <phk@FreeBSD.ORG>
 Oncore driver (Original author)
 Frank Kardel <kardel@ntp.org>
 PARSE <GENERIC> driver (>14 reference clocks), STREAMS modules for
 PARSE, support scripts, syslog cleanup, dynamic interface handling
 Dave Katz <dkatz@cisco.com>
 RS/6000 AIX port
 Johannes Maximilian Kuehn <kuehn@ntp.org>
 Rewrote sntp (now ntpdig) to comply with NTPv4 specification
 Clayton Kirkwood <kirkwood@striderfm.intel.com>
 port to WindowsNT 3.5
 Hans Lambermont <Hans.Lambermont@nl.origin-it.com>
 <H.Lambermont@chello.nl>
 ntpsweep
 Craig Leres <leres@ee.lbl.gov>
 4.4BSD port, ppsclock, Magnavox GPS clock driver
 George Lindholm <lindholm@ucs.ubc.ca>
 SunOS 5.1 port
 Louis A. Mamakos <louie@ni.umd.edu>
 MD5-based authentication
 Lars H. Mathiesen <thorinn@diku.dk>
 adaptation of foundation code for Version 3 as specified in
 RFC-1305
 Danny Mayer <mayer@ntp.org>
 Network I/O, Windows Port, Code Maintenance
 Gary E. Miller <gem@rellim.com>
 Code ANSIfication, testing, ntpviz
 David L. Mills
 Version 4 foundation, precision kernel
 clock drivers: 1, 3, 4, 6, 7, 11, 13, 18, 19, 22, 36
 Wolfgang Moeller <moeller@gwdgv1.dnet.gwdg.de>
 VMS port
 Jeffrey Mogul <mogul@pa.dec.com>
 ntptrace utility
 Tom Moore <tmoore@fievel.daytonoh.ncr.com>
 i386 svr4 port
 Kamal A Mostafa <kamal@whence.com>
 SCO OpenServer port
 Derek Mulcahy <derek@toybox.demon.co.uk>
 ARCRON MSF clock driver
 Hal Murray <hmurray@megapathdsl.net>
 Code cleanups, bug hunting, sage advice
 Rob Neal <neal@ntp.org>
 code maintenance
 Rainer Pruy <Rainer.Pruy@informatik.uni-erlangen.de>
 monitoring/trap scripts, statistics file handling
 Eric S. Raymond <esr@thyrsus.com>
 Massive code reduction/refactoring/hardening
 documentation cleanup and update
 new refclock configuration syntax
 Dirce Richards <dirce@zk3.dec.com>
 Digital UNIX V4.0 port
 Wilfredo Sánchez <wsanchez@apple.com>
 added support for NetInfo
 Nick Sayer <mrapple@quack.kfu.com>
 SunOS streams modules
 Jack Sasportas <jack@innovativeinternet.com>
 Saved a Lot of space on the stuff in the html/pic/ subdirectory
 Ray Schnitzler <schnitz@unipress.com>
 Unixware1 port
 Matt Selsky <matthew.selsky@twosigma.com>
 Documentation corrections, compiler warning and port cleanup
 Michael Shields <shields@tembel.org>
 USNO clock driver
 Jack Sasportas <jack@innovativeinternet.com>
 Saved a Lot of space on the stuff in the html/pic/ subdirectory
 Michael Shields <shields@tembel.org>
 USNO clock driver
 Jeff Steinman <jss@pebbles.jpl.nasa.gov>
 Datum PTS clock driver
 Harlan Stenn <harlan@pfcs.com>
 GNU automake/autoconfigure makeover, various other bits
 (see the ChangeLog)
 Kenneth Stone <ken@sdd.hp.com>
 HP-UX port
 Amar Takhar <verm@darkbeer.org>
 waf build recipe
 Ajit Thyagarajan <ajit@ee.udel.edu>
 IP multicast/anycast support
 Tomoaki TSURUOKA <tsuruoka@nc.fukuoka-u.ac.jp>
 TRAK clock driver
 Brian Utterback <brian.utterback@oracle.com>
 General codebase
 Loganaden Velvindron <loganaden@gmail.com>
 Sandboxing (libseccomp) support
 Paul A Vixie <vixie@vix.com>
 TrueTime GPS driver, generic TrueTime clock driver
 Ulrich Windl <Ulrich.Windl@rz.uni-regensburg.de>
 corrected and validated HTML documents according to the HTML DTD
License: NTP

Files: ntpd/ntp_signd.c
Copyright: Andrew Tridgell
 NTPsec project contributors
 Red Hat, Inc.
License: NTP
